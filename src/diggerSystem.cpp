#include "../include/diggerSystem.h"

DiggerSystem::DiggerSystem(string usbLocation, int hammer_direction_relay, int hammer_power_relay, int bucketHeight_direction_relay, int bucketHeight_power_relay, int bucketAngle_direction_relay, int bucketAngle_power_relay)
{
    usbRelay = serialPort(usbLocation);

    hammerHeight = Actuator(hammer_power_relay, hammer_direction_relay);

    bucketHeight = Actuator(bucketHeight_power_relay, bucketHeight_direction_relay);
    bucketAngle = Actuator(bucketAngle_power_relay, bucketAngle_direction_relay);
}