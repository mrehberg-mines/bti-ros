#include "../include/actuator.h"

Actuator::Actuator(serialPort comPort, int power_relay, int direction_relay)
{
    com = comPort;

    powerRelay = power_relay;
    directionRelay = direction_relay;
}

void Actuator::moveIn()
{
    directionOut = true;
    powerOut = true;
    position -= stepSize;
    com.write("relay on " + directionRelay);
    com.write("relay on " + powerRelay);
    com.write("position " + position);
}

void Actuator::moveOut()
{
    directionOut = false;
    powerOut = true;
    position += stepSize;
    com.write("relay off " + directionRelay);
    com.write("relay on " + powerRelay);
    com.write("position " + position);

}

void Actuator::gotoPosition()
{
    if (position < targetPosition)
    {
        do{
        moveOut()
        } while (position < targetPosition)  
    }
    else
    {
       do{
        moveIn()
        } while (position > targetPosition)   
    }
}


void Actuator::stop()
{
    powerOut = false;
    com.write("relay off " + powerRelay);
}

void Actuator::resetPosition()
{
    position = resetValue;
}


void Actuator::reportPosition()
{
    return position
}


string Actuator::getDirection()
{
    if (directionOut)
    {
        return ("IN");
    }
    else
    {
        return ("OUT")
    }
}

bool getPowerOut()
{
    return powerOut;
}

bool getDirectionOut()
{
    return directionOut;
}