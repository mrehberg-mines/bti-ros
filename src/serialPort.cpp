#include "../include/serialPort.h"

serialPort::serialPort(string portName)
{
    serial_port = open(portName, O_RDWR);

    cfsetispeed(&tty, B19200);
    cfsetospeed(&tty, B19200);

    if (tcgetattr(serial_port, &tty) != 0)
    {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
        return 1;
    }
}

serialPort::serialPort(string portName, int baudRate)
{
    serial_port = open(portName, O_RDWR);

    cfsetispeed(&tty, baudRate);
    cfsetospeed(&tty, baudRate);

    if (tcgetattr(serial_port, &tty) != 0)
    {
        printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
        return 1;
    }
}

void serialPort::write(string toWrite)
{
    write(serial_port, 0x0D, 1);
    write(serial_port, toWrite.c_str(), sizeof(toWrite.c_str()));
    write(serial_port, 0x0D, 1);
}