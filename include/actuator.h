// actuator.h
#ifndef ACTUATOR_H
#define ACTUATOR_H

#include "serialPort.h"

class Actuator
{
public:
    Actuator(serialPort comPort, int power_relay, int direction_relay, float resetValue);

    void moveIn();
    void moveOut();
    void stop();
    void resetPosition();

    string getDirection();

    bool getPowerOut();
    bool getDirectionOut();
    float reportPosition()

private:
    serialPort com;

    int powerRelay;
    bool powerOut = false;
    float position = 0.0; // cm
    float stepSize = 0.001; //cm per count ?
    float resetValue;
    float minPosition = 0; 
    float maxPosition = 10; // cm ? 
    float targetPosition = 0; // cm


    int directionRelay;
    bool directionOut = false;
};

#endif