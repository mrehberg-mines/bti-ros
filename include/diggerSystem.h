#include <string.h>

#include "../serialPort.h"
#include "../actuator.h"

class DiggerSystem
{
public:
    DiggerSystem(string usbLoc, int hammer_direction_relay, int hammer_power_relay, int bucketHeight_direction_relay, int bucketHeight_power_relay, int bucketAngle_direction_relay, int bucketAngle_power_relay);

private:
    serialPort usbRelay;

    Actuator hammerHeight;

    Actuator bucketHeight;
    Actuator bucketAngle;
};