// serialPort.h

#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <stdio.h>
#include <string.h>
#include <cstring>

#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

class serialPort
{
public:
    serialPort(string portName);
    serialPort(string portName, int baudRate);

    void write(string toWrite);
    string read();

private:
    int serial_port;
    struct termios tty;
};

#endif